#include <rdiscover.h>
#include <assert.h>

#define PROTOCOL_NAME	"RDP\0"

/*
 * Creates a packet with the specified `topic`. `response` is a boolean value
 * specifying whether a response should be sent back.
 * The function returns a pointer to the data and sets its length into `len`.
 * The return value must be later freed using function `free` to avoid memory
 * leaks.
 */
void *rdiscover_make_packet(int response, const char *topic, size_t *len)
{
	rdiscover_packet_t *p;
	size_t topic_len;

	assert(len);
	topic_len = (topic ? strlen(topic) : 0);
	*len = sizeof(rdiscover_packet_t) + topic_len;
	if(!(p = malloc(sizeof(rdiscover_packet_t) + topic_len)))
		return NULL;
	memcpy(&p->protocol, PROTOCOL_NAME, sizeof(p->protocol));
	p->response = response;
	p->topic_len = topic_len;
	if(topic)
		memcpy(&p->topic, topic, topic_len);
	return p;
}
