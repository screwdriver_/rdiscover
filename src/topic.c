#include <rdiscover.h>
#include <assert.h>

/*
 * Checks whether the given `pattern` matches the given `topic`.
 */
// TODO `^`
// TODO Unit test
int rdiscover_topic_match(const char *topic, const char *pattern)
{
	assert(topic && pattern);
	while(*topic && *pattern)
	{
		if(*pattern == '\\')
			++pattern;
		else if(*pattern == '*')
			return rdiscover_topic_match(topic + 1, pattern)
				|| rdiscover_topic_match(topic, pattern + 1);
		else if(*pattern == '$')
			break;
		if(!*pattern)
			return 1;
		if(*topic != *pattern)
			return 0;
		++topic;
		++pattern;
	}
	if(pattern[0] == '$')
	{
		if(!pattern[1])
			return !*topic;
		else
			return 0;
	}
	return !*pattern;
}
