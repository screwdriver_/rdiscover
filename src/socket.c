#include <rdiscover.h>
#include <assert.h>
#include <fcntl.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#ifndef RDISCOVER_FREESTANDING
/*
 * Creates a UDP socket on port `port` and returns it.
 */
int rdiscover_create_socket(uint16_t port)
{
	int sock;
	struct sockaddr_in addr;

	if((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
		return -1;
	bzero(&addr, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_port = htons(port);
	if(bind(sock, (const struct sockaddr *) &addr, sizeof(addr)) < 0)
	{
		close(sock);
		return -1;
	}
	return sock;
}

/*
 * Receives a packet from socket `sock`. The function shall set the `host` and
 * `topic` fields and return 1 if a packet is received. If no packet is
 * received, the function returns 0. On error, the function returns -1.
 */
int rdiscover_read(int sock, char *host, char *topic)
{
	//ssize_t n;
	//struct sockaddr_in addr;

	assert(sock >= 0);
	// TODO Peek to check amount of data
	// TODO return 0; if not enough data
	/*if((n = recvfrom(sock, )) < 0)
		return 0;*/
	if(host)
	{
		// TODO If host is not NULL, set it
	}
	if(topic)
	{
		// TODO If topic is not NULL, set it
	}
	return 1;
}

/*
 * Writes a packet on socket `sock` to the given `host` with the given `topic`.
 * `response` is a boolean value specifying whether a response should be sent
 * back.
 */
void rdiscover_write(int sock, const char *host, int response,
	const char *topic)
{
	void *data;
	size_t len;
	struct sockaddr_in addr;

	assert(sock >= 0 && host);
	data = rdiscover_make_packet(response, topic, &len);
	bzero(&addr, sizeof(addr));
	addr.sin_family = AF_INET;
	// TODO Fill `addr`
	sendto(sock, data, len, 0, (const struct sockaddr *) &addr, sizeof(addr));
	free(data);
}
#endif
