#ifndef RDISCOVER_H
# define RDISCOVER_H

# include <stdint.h>
# include <string.h>

/*
 * Structure representing a RDiscover packet.
 */
typedef struct
{
	char protocol[4];
	char response;
	uint16_t topic_len;
	char topic[0];
} rdiscover_packet_t;

void *rdiscover_make_packet(int response, const char *topic, size_t *len);
int rdiscover_topic_match(const char *topic, const char *pattern);

# ifndef RDISCOVER_FREESTANDING
int rdiscover_create_socket(uint16_t port);
int rdiscover_read(int sock, char *host, char *topic);
void rdiscover_write(int sock, const char *host, int response,
	const char *topic);
# endif

extern void *malloc(size_t size);
extern void free(void *ptr);
extern void *memcpy(void *dest, const void *src, size_t n);

#endif
