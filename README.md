# RDiscover

RDiscover is lightweight protocol based on UDP which allows to discover other devices on the local network that implement a RDiscover listener.
This implementation holds a stub for freestanding RDiscover and an implementation for POSIX systems.



## Compilation option

To enable freestanding stub, the macro RDISCOVER_FREESTANDING must be set.

**Note**: The freestanding stub requires an implementation of `strlen`, `memcpy`, `malloc` and `free` functions.



## Protocol overview

The protocol works by sending a packet on the broadcast address of the network, then every devices receiving the packet send a response back directly to the sender.
The port used for communications is `4452`.



### Packets

The protocol has only one packet type:

- **discover**: Sent on the broadcast address to require a list of devices

Format:

| Field name | Field size  | Description/value                                                            |
|------------|-------------|------------------------------------------------------------------------------|
| protocol   | 4           | `RDP0` (**RD**iscover **P**rotocol + the version number in binary)           |
| response   | 1           | Boolean telling whether the sender requires a response or not                |
| topic_len  | 2           | The length of the topic string                                               |
| topic      | `topic_len` | A string of length `topic_len` representing the topic required by the client |

A topic is a string of any characters associated with the device.
Topics support pattern matching (see related section).
Every discoverable devices which has at least one matching pattern SHOULD send a response.
If the pattern matches several topics, the behaviour is implementation defined.

If the `response` field is not set, the receiving device MUST NOT send a response.
A response packet MUST NOT have the `response` field set.



### Topics pattern matching

Topics support pattern matching with the following characters:
- `*`: Matches any number of any characters
- `^`: Matches if the characters after are at the beginning of the string
- `$`: Matches if the characters before are at the end of the string
- `\\`: Escapes the next character
